﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindGoes6.Data;

namespace TestProject
{
    class CSVTest
    {
        public static void Test1()
        {
            string csvfile = Path.Combine(Common.DataFolder, "Data_Q0690_211762_20160307_175739.csv");
            CSV csv = new CSV(csvfile);
            csv[0, 1] = "Hello";
            csv.AddRow(new string []{ "a", "Hi"});
            csv.AddColumn();
            Console.WriteLine(csv.ToString());
        }
    }
}
