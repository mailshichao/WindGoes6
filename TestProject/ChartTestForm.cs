﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace TestProject
{
    public partial class ChartTestForm : Form
    {
        Random rnd = new Random(0);
        public ChartTestForm()
        {
            InitializeComponent();
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            foreach (var sers in chart1.Series)
            {
                double v0 = sers.Points[0].YValues[0];
                v0 += rnd.NextDouble() * 6 - 3;
                v0 = double.Parse(v0.ToString("0.00"));
                for (int i = 99; i > 0; i--)
                {
                    sers.Points[i] = new DataPoint(0, sers.Points[i - 1].YValues[0]);
                }
                sers.Points[0] = new DataPoint(0, v0);
            }

            // Console.WriteLine(1);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Initialize(chart1);
            timer1.Start();
        }

        private void Initialize(Chart ch1)
        {
            foreach (var sers in ch1.Series)
            {
                sers.Points.Clear();
                for (int i = 0; i < 100; i++)
                {
                    sers.Points.AddXY(i, rnd.Next(50, 100));
                }
            }
        }

    }
}
