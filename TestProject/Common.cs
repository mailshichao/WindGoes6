﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProject
{
	class Common
	{
		/// <summary>
		/// 默认的数据文件夹。
		/// </summary>
		public static string DataFolder { get; set; } = @"..\..\Data";

        /// <summary>
        /// 给定文件名，返回数据文件夹中的数据的绝对路径。
        /// </summary>
        /// <param name="filepath"></param>
        /// <returns></returns>
        public static string GetDataPath(string filepath)
        {
            return Path.GetFullPath(Path.Combine(DataFolder, filepath));
        }

        internal static string TranslateInfo(string info)
        {
            return info.Replace("主机名", "Computer Name")
                .Replace("OS 名称", "OS")
                .Replace("版本", "Version")
                .Replace("OS 制造商", "OS Maker")
                .Replace("状态", "Status");
        }

        internal static byte[] ReadFilePart(string filepath, int start, int length)
        {
            byte[] bts = null;

            if (!File.Exists(filepath))
                return bts;

            FileStream fs = new FileStream(filepath, FileMode.Open, FileAccess.Read);
            if(start >= fs.Length)
            {
                fs.Close();
                return bts;
            }

            fs.Seek(start, SeekOrigin.Begin);
            int end = start + length >= fs.Length ? (int) fs.Length : start + length;

            bts = new byte[end - start];
            fs.Read(bts, start, bts.Length);
            return bts; 
        }
    }
}
