﻿namespace TestProject
{
	partial class Form1
	{
		/// <summary>
		/// 必需的设计器变量。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows 窗体设计器生成的代码

		/// <summary>
		/// 设计器支持所需的方法 - 不要修改
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnCSVTest = new System.Windows.Forms.Button();
            this.btnBalloonTest = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabConfig = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.colKey = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnWriteConfig = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.btnReadConfig = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.btnConfigAccessTest = new System.Windows.Forms.Button();
            this.btnSendFilePartHttp = new System.Windows.Forms.Button();
            this.btnInvokeWebApi = new System.Windows.Forms.Button();
            this.btnAESTest = new System.Windows.Forms.Button();
            this.btnConFormTest = new System.Windows.Forms.Button();
            this.btnCSVHelperTest = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnMoveScroll = new System.Windows.Forms.Button();
            this.panelScrollTest = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.btnGetSystemInformation = new System.Windows.Forms.Button();
            this.btnGetOSVersion = new System.Windows.Forms.Button();
            this.btnXMLWrite = new System.Windows.Forms.Button();
            this.btnFortuneSimulator = new System.Windows.Forms.Button();
            this.btnCommandCommandParamterTest = new System.Windows.Forms.Button();
            this.btnAnalyzeNovel = new System.Windows.Forms.Button();
            this.btnXMLDemo = new System.Windows.Forms.Button();
            this.btnHardwareInfo = new System.Windows.Forms.Button();
            this.btnCommandCommandTest = new System.Windows.Forms.Button();
            this.btn_DosInvoker = new System.Windows.Forms.Button();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.txtCommondResult = new System.Windows.Forms.TextBox();
            this.btnDosCommondTest = new System.Windows.Forms.Button();
            this.txtDosCommand = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabConfig.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panelScrollTest.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCSVTest
            // 
            this.btnCSVTest.Location = new System.Drawing.Point(204, 18);
            this.btnCSVTest.Name = "btnCSVTest";
            this.btnCSVTest.Size = new System.Drawing.Size(163, 46);
            this.btnCSVTest.TabIndex = 0;
            this.btnCSVTest.Text = "CSV Test";
            this.btnCSVTest.UseVisualStyleBackColor = true;
            this.btnCSVTest.Click += new System.EventHandler(this.btnCSVTest_Click);
            // 
            // btnBalloonTest
            // 
            this.btnBalloonTest.Location = new System.Drawing.Point(61, 17);
            this.btnBalloonTest.Name = "btnBalloonTest";
            this.btnBalloonTest.Size = new System.Drawing.Size(154, 35);
            this.btnBalloonTest.TabIndex = 1;
            this.btnBalloonTest.Text = "Balloon Test";
            this.btnBalloonTest.UseVisualStyleBackColor = true;
            this.btnBalloonTest.Click += new System.EventHandler(this.BtnBalloonTest_Click);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(119, 18);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(220, 21);
            this.textBox1.TabIndex = 2;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(6, 17);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(107, 20);
            this.button2.TabIndex = 3;
            this.button2.Text = "Add Button";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabConfig);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(776, 385);
            this.tabControl1.TabIndex = 4;
            // 
            // tabConfig
            // 
            this.tabConfig.Controls.Add(this.label2);
            this.tabConfig.Controls.Add(this.label1);
            this.tabConfig.Controls.Add(this.dataGridView1);
            this.tabConfig.Controls.Add(this.btnWriteConfig);
            this.tabConfig.Controls.Add(this.textBox2);
            this.tabConfig.Controls.Add(this.btnReadConfig);
            this.tabConfig.Location = new System.Drawing.Point(4, 22);
            this.tabConfig.Name = "tabConfig";
            this.tabConfig.Padding = new System.Windows.Forms.Padding(3);
            this.tabConfig.Size = new System.Drawing.Size(768, 359);
            this.tabConfig.TabIndex = 5;
            this.tabConfig.Text = "日志文件读写";
            this.tabConfig.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 341);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(215, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "配置文件位置：程序目录下 config.ini";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(295, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "说明";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colKey,
            this.colValue});
            this.dataGridView1.Location = new System.Drawing.Point(16, 65);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(275, 269);
            this.dataGridView1.TabIndex = 3;
            // 
            // colKey
            // 
            this.colKey.HeaderText = "Key";
            this.colKey.Name = "colKey";
            // 
            // colValue
            // 
            this.colValue.HeaderText = "Value";
            this.colValue.Name = "colValue";
            // 
            // btnWriteConfig
            // 
            this.btnWriteConfig.Location = new System.Drawing.Point(121, 23);
            this.btnWriteConfig.Name = "btnWriteConfig";
            this.btnWriteConfig.Size = new System.Drawing.Size(99, 36);
            this.btnWriteConfig.TabIndex = 2;
            this.btnWriteConfig.Text = "写";
            this.btnWriteConfig.UseVisualStyleBackColor = true;
            this.btnWriteConfig.Click += new System.EventHandler(this.BtnWriteConfig_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(297, 23);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(455, 311);
            this.textBox2.TabIndex = 1;
            this.textBox2.Text = resources.GetString("textBox2.Text");
            // 
            // btnReadConfig
            // 
            this.btnReadConfig.Location = new System.Drawing.Point(16, 23);
            this.btnReadConfig.Name = "btnReadConfig";
            this.btnReadConfig.Size = new System.Drawing.Size(99, 36);
            this.btnReadConfig.TabIndex = 0;
            this.btnReadConfig.Text = "读";
            this.btnReadConfig.UseVisualStyleBackColor = true;
            this.btnReadConfig.Click += new System.EventHandler(this.BtnReadConfig_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.btnConfigAccessTest);
            this.tabPage1.Controls.Add(this.btnSendFilePartHttp);
            this.tabPage1.Controls.Add(this.btnInvokeWebApi);
            this.tabPage1.Controls.Add(this.btnAESTest);
            this.tabPage1.Controls.Add(this.btnConFormTest);
            this.tabPage1.Controls.Add(this.btnCSVHelperTest);
            this.tabPage1.Controls.Add(this.btnCSVTest);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(768, 359);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(204, 131);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(163, 41);
            this.button1.TabIndex = 13;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnConfigAccessTest
            // 
            this.btnConfigAccessTest.Location = new System.Drawing.Point(392, 18);
            this.btnConfigAccessTest.Name = "btnConfigAccessTest";
            this.btnConfigAccessTest.Size = new System.Drawing.Size(163, 46);
            this.btnConfigAccessTest.TabIndex = 12;
            this.btnConfigAccessTest.Text = "ConfigAccess测试";
            this.btnConfigAccessTest.UseVisualStyleBackColor = true;
            this.btnConfigAccessTest.Click += new System.EventHandler(this.BtnConfigAccessTest_Click);
            // 
            // btnSendFilePartHttp
            // 
            this.btnSendFilePartHttp.Location = new System.Drawing.Point(392, 74);
            this.btnSendFilePartHttp.Name = "btnSendFilePartHttp";
            this.btnSendFilePartHttp.Size = new System.Drawing.Size(163, 46);
            this.btnSendFilePartHttp.TabIndex = 11;
            this.btnSendFilePartHttp.Text = "HTTP Send File Part";
            this.btnSendFilePartHttp.UseVisualStyleBackColor = true;
            this.btnSendFilePartHttp.Click += new System.EventHandler(this.btnSendFilePartHttp_Click);
            // 
            // btnInvokeWebApi
            // 
            this.btnInvokeWebApi.Location = new System.Drawing.Point(204, 74);
            this.btnInvokeWebApi.Name = "btnInvokeWebApi";
            this.btnInvokeWebApi.Size = new System.Drawing.Size(163, 46);
            this.btnInvokeWebApi.TabIndex = 10;
            this.btnInvokeWebApi.Text = "Invoke Web Api";
            this.btnInvokeWebApi.UseVisualStyleBackColor = true;
            this.btnInvokeWebApi.Click += new System.EventHandler(this.BtnInvokeWebApi_Click);
            // 
            // btnAESTest
            // 
            this.btnAESTest.Location = new System.Drawing.Point(23, 126);
            this.btnAESTest.Name = "btnAESTest";
            this.btnAESTest.Size = new System.Drawing.Size(163, 46);
            this.btnAESTest.TabIndex = 8;
            this.btnAESTest.Text = "AES 测试";
            this.btnAESTest.UseVisualStyleBackColor = true;
            this.btnAESTest.Click += new System.EventHandler(this.btnAESTest_Click);
            // 
            // btnConFormTest
            // 
            this.btnConFormTest.Location = new System.Drawing.Point(23, 74);
            this.btnConFormTest.Name = "btnConFormTest";
            this.btnConFormTest.Size = new System.Drawing.Size(163, 46);
            this.btnConFormTest.TabIndex = 2;
            this.btnConFormTest.Text = "Connection Form Test";
            this.btnConFormTest.UseVisualStyleBackColor = true;
            this.btnConFormTest.Click += new System.EventHandler(this.BtnConFormTest_Click);
            // 
            // btnCSVHelperTest
            // 
            this.btnCSVHelperTest.Location = new System.Drawing.Point(23, 18);
            this.btnCSVHelperTest.Name = "btnCSVHelperTest";
            this.btnCSVHelperTest.Size = new System.Drawing.Size(163, 46);
            this.btnCSVHelperTest.TabIndex = 1;
            this.btnCSVHelperTest.Text = "CSVHelper Test";
            this.btnCSVHelperTest.UseVisualStyleBackColor = true;
            this.btnCSVHelperTest.Click += new System.EventHandler(this.BtnCSVHelperTest_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.textBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(768, 359);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnMoveScroll);
            this.tabPage3.Controls.Add(this.panelScrollTest);
            this.tabPage3.Controls.Add(this.btnBalloonTest);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(768, 359);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "tabPage3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnMoveScroll
            // 
            this.btnMoveScroll.Location = new System.Drawing.Point(61, 58);
            this.btnMoveScroll.Name = "btnMoveScroll";
            this.btnMoveScroll.Size = new System.Drawing.Size(154, 36);
            this.btnMoveScroll.TabIndex = 3;
            this.btnMoveScroll.Text = "Panel.ScrollBar";
            this.btnMoveScroll.UseVisualStyleBackColor = true;
            this.btnMoveScroll.Click += new System.EventHandler(this.btnMoveScroll_Click);
            // 
            // panelScrollTest
            // 
            this.panelScrollTest.AutoScroll = true;
            this.panelScrollTest.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelScrollTest.Controls.Add(this.groupBox2);
            this.panelScrollTest.Controls.Add(this.groupBox1);
            this.panelScrollTest.Location = new System.Drawing.Point(325, 47);
            this.panelScrollTest.Name = "panelScrollTest";
            this.panelScrollTest.Size = new System.Drawing.Size(286, 215);
            this.panelScrollTest.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Location = new System.Drawing.Point(53, 305);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(190, 124);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBox2";
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(53, 143);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(190, 124);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.button3);
            this.tabPage4.Controls.Add(this.btnGetSystemInformation);
            this.tabPage4.Controls.Add(this.btnGetOSVersion);
            this.tabPage4.Controls.Add(this.btnXMLWrite);
            this.tabPage4.Controls.Add(this.btnFortuneSimulator);
            this.tabPage4.Controls.Add(this.btnCommandCommandParamterTest);
            this.tabPage4.Controls.Add(this.btnAnalyzeNovel);
            this.tabPage4.Controls.Add(this.btnXMLDemo);
            this.tabPage4.Controls.Add(this.btnHardwareInfo);
            this.tabPage4.Controls.Add(this.btnCommandCommandTest);
            this.tabPage4.Controls.Add(this.btn_DosInvoker);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(768, 359);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "其他";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // btnGetSystemInformation
            // 
            this.btnGetSystemInformation.Location = new System.Drawing.Point(391, 83);
            this.btnGetSystemInformation.Name = "btnGetSystemInformation";
            this.btnGetSystemInformation.Size = new System.Drawing.Size(169, 52);
            this.btnGetSystemInformation.TabIndex = 9;
            this.btnGetSystemInformation.Text = "Get System Information";
            this.btnGetSystemInformation.UseVisualStyleBackColor = true;
            this.btnGetSystemInformation.Click += new System.EventHandler(this.BtnGetSystemInformation_Click);
            // 
            // btnGetOSVersion
            // 
            this.btnGetOSVersion.Location = new System.Drawing.Point(391, 18);
            this.btnGetOSVersion.Name = "btnGetOSVersion";
            this.btnGetOSVersion.Size = new System.Drawing.Size(169, 52);
            this.btnGetOSVersion.TabIndex = 8;
            this.btnGetOSVersion.Text = "GetOSVersion";
            this.btnGetOSVersion.UseVisualStyleBackColor = true;
            this.btnGetOSVersion.Click += new System.EventHandler(this.BtnGetOSVersion_Click);
            // 
            // btnXMLWrite
            // 
            this.btnXMLWrite.Location = new System.Drawing.Point(207, 83);
            this.btnXMLWrite.Name = "btnXMLWrite";
            this.btnXMLWrite.Size = new System.Drawing.Size(169, 52);
            this.btnXMLWrite.TabIndex = 7;
            this.btnXMLWrite.Text = "XML Write Demo";
            this.btnXMLWrite.UseVisualStyleBackColor = true;
            this.btnXMLWrite.Click += new System.EventHandler(this.BtnXMLWrite_Click);
            // 
            // btnFortuneSimulator
            // 
            this.btnFortuneSimulator.Location = new System.Drawing.Point(21, 222);
            this.btnFortuneSimulator.Name = "btnFortuneSimulator";
            this.btnFortuneSimulator.Size = new System.Drawing.Size(169, 52);
            this.btnFortuneSimulator.TabIndex = 6;
            this.btnFortuneSimulator.Text = "Fortune Simulator";
            this.btnFortuneSimulator.UseVisualStyleBackColor = true;
            this.btnFortuneSimulator.Click += new System.EventHandler(this.BtnFortuneSimulator_Click);
            // 
            // btnCommandCommandParamterTest
            // 
            this.btnCommandCommandParamterTest.Location = new System.Drawing.Point(21, 152);
            this.btnCommandCommandParamterTest.Name = "btnCommandCommandParamterTest";
            this.btnCommandCommandParamterTest.Size = new System.Drawing.Size(169, 52);
            this.btnCommandCommandParamterTest.TabIndex = 5;
            this.btnCommandCommandParamterTest.Text = "Common Command with Invoke Paramter";
            this.btnCommandCommandParamterTest.UseVisualStyleBackColor = true;
            this.btnCommandCommandParamterTest.Click += new System.EventHandler(this.BtnCommandCommandParamterTest_Click);
            // 
            // btnAnalyzeNovel
            // 
            this.btnAnalyzeNovel.Location = new System.Drawing.Point(207, 222);
            this.btnAnalyzeNovel.Name = "btnAnalyzeNovel";
            this.btnAnalyzeNovel.Size = new System.Drawing.Size(169, 52);
            this.btnAnalyzeNovel.TabIndex = 4;
            this.btnAnalyzeNovel.Text = "Analyze Novel";
            this.btnAnalyzeNovel.UseVisualStyleBackColor = true;
            this.btnAnalyzeNovel.Click += new System.EventHandler(this.BtnAnalyzeNovel_Click);
            // 
            // btnXMLDemo
            // 
            this.btnXMLDemo.Location = new System.Drawing.Point(207, 18);
            this.btnXMLDemo.Name = "btnXMLDemo";
            this.btnXMLDemo.Size = new System.Drawing.Size(169, 52);
            this.btnXMLDemo.TabIndex = 3;
            this.btnXMLDemo.Text = "XML Read Demo";
            this.btnXMLDemo.UseVisualStyleBackColor = true;
            this.btnXMLDemo.Click += new System.EventHandler(this.BtnXMLDemo_Click);
            // 
            // btnHardwareInfo
            // 
            this.btnHardwareInfo.Location = new System.Drawing.Point(207, 152);
            this.btnHardwareInfo.Name = "btnHardwareInfo";
            this.btnHardwareInfo.Size = new System.Drawing.Size(169, 52);
            this.btnHardwareInfo.TabIndex = 2;
            this.btnHardwareInfo.Text = "Get Hardware Info";
            this.btnHardwareInfo.UseVisualStyleBackColor = true;
            this.btnHardwareInfo.Click += new System.EventHandler(this.BtnHardwareInfo_Click);
            // 
            // btnCommandCommandTest
            // 
            this.btnCommandCommandTest.Location = new System.Drawing.Point(21, 83);
            this.btnCommandCommandTest.Name = "btnCommandCommandTest";
            this.btnCommandCommandTest.Size = new System.Drawing.Size(169, 52);
            this.btnCommandCommandTest.TabIndex = 1;
            this.btnCommandCommandTest.Text = "Common Command Invoke";
            this.btnCommandCommandTest.UseVisualStyleBackColor = true;
            this.btnCommandCommandTest.Click += new System.EventHandler(this.BtnCommandCommandTest_Click);
            // 
            // btn_DosInvoker
            // 
            this.btn_DosInvoker.Location = new System.Drawing.Point(21, 18);
            this.btn_DosInvoker.Name = "btn_DosInvoker";
            this.btn_DosInvoker.Size = new System.Drawing.Size(169, 52);
            this.btn_DosInvoker.TabIndex = 0;
            this.btn_DosInvoker.Text = "Dos Command Invoke";
            this.btn_DosInvoker.UseVisualStyleBackColor = true;
            this.btn_DosInvoker.Click += new System.EventHandler(this.Btn_DosInvoker_Click);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.txtCommondResult);
            this.tabPage5.Controls.Add(this.btnDosCommondTest);
            this.tabPage5.Controls.Add(this.txtDosCommand);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(768, 359);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "远程调用测试";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // txtCommondResult
            // 
            this.txtCommondResult.Location = new System.Drawing.Point(16, 46);
            this.txtCommondResult.Multiline = true;
            this.txtCommondResult.Name = "txtCommondResult";
            this.txtCommondResult.Size = new System.Drawing.Size(736, 307);
            this.txtCommondResult.TabIndex = 2;
            // 
            // btnDosCommondTest
            // 
            this.btnDosCommondTest.Location = new System.Drawing.Point(690, 19);
            this.btnDosCommondTest.Name = "btnDosCommondTest";
            this.btnDosCommondTest.Size = new System.Drawing.Size(62, 21);
            this.btnDosCommondTest.TabIndex = 1;
            this.btnDosCommondTest.Text = "Run";
            this.btnDosCommondTest.UseVisualStyleBackColor = true;
            this.btnDosCommondTest.Click += new System.EventHandler(this.BtnDosCommondTest_Click);
            // 
            // txtDosCommand
            // 
            this.txtDosCommand.Location = new System.Drawing.Point(16, 19);
            this.txtDosCommand.Name = "txtDosCommand";
            this.txtDosCommand.Size = new System.Drawing.Size(668, 21);
            this.txtDosCommand.TabIndex = 0;
            this.txtDosCommand.Text = "dir c:\\";
            this.txtDosCommand.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtDosCommand_KeyDown);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(391, 152);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(169, 52);
            this.button3.TabIndex = 10;
            this.button3.Text = "ConfigAccess测试";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "测试窗体";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabConfig.ResumeLayout(false);
            this.tabConfig.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.panelScrollTest.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnCSVTest;
        private System.Windows.Forms.Button btnBalloonTest;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnCSVHelperTest;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button btn_DosInvoker;
        private System.Windows.Forms.Button btnCommandCommandTest;
        private System.Windows.Forms.Button btnHardwareInfo;
        private System.Windows.Forms.Button btnConFormTest;
        private System.Windows.Forms.Button btnXMLDemo;
        private System.Windows.Forms.Button btnAnalyzeNovel;
        private System.Windows.Forms.Button btnCommandCommandParamterTest;
        private System.Windows.Forms.Button btnFortuneSimulator;
        private System.Windows.Forms.Button btnXMLWrite;
        private System.Windows.Forms.Button btnAESTest;
        private System.Windows.Forms.Panel panelScrollTest;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnMoveScroll;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnGetOSVersion;
        private System.Windows.Forms.Button btnGetSystemInformation;
        private System.Windows.Forms.Button btnInvokeWebApi;
        private System.Windows.Forms.Button btnSendFilePartHttp;
        private System.Windows.Forms.Button btnConfigAccessTest;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TextBox txtCommondResult;
        private System.Windows.Forms.Button btnDosCommondTest;
        private System.Windows.Forms.TextBox txtDosCommand;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabPage tabConfig;
        private System.Windows.Forms.Button btnReadConfig;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colKey;
        private System.Windows.Forms.DataGridViewTextBoxColumn colValue;
        private System.Windows.Forms.Button btnWriteConfig;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button3;
    }
}

