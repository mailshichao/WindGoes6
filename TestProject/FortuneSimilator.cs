﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProject
{
    class FortuneSimilator
    {
        public void Test()
        {
            double[] ps = new double[100];
            double initMoney = 10000;
            for (int i = 0; i < ps.Length; i++)
            {
                ps[i] = initMoney;
            }

            Random rnd = new Random();

            for (int i = 0; i < 70000; i++)
            {

                //for (int p = 0; p < ps.Length; p++)
                //    ps[i] *= 1.0001;

                for (int p = 0; p < ps.Length; p++)
                {
                    int pos = rnd.Next(ps.Length);
                    ps[p] -= 1;
                    ps[pos] += 1;
                }
            }

            foreach (var item in ps.GroupBy(p => p).OrderByDescending(p => p.Key).Take(50))
            {
                Console.WriteLine(item.Key + ":" + item.Count());
            }

        }
    }
}
