﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace WindGoes6
{
    /// <summary>
    /// 获得CPU的信息。
    /// </summary>
    public class ComputerInfo
    {
        /// <summary>
        /// 返回CPU的ID，格式为16位字母加数字，如 BFEBFBFF000206D7
        /// </summary>
        /// <returns></returns>
        public static string GetCPUInfo()
        {
            return GetHardWareInfo("Win32_Processor", "ProcessorId");
        }

        /// <summary>
        /// 获得BIOS序列号。
        /// </summary>
        /// <returns></returns>
        public static string GetBIOSInfo()
        {
            return GetHardWareInfo("Win32_BIOS", "SerialNumber");
        }

        /// <summary>
        /// 获得主板编号，格式如 113025580000010
        /// </summary>
        /// <returns></returns>
        public static string GetMotherBoardInfo()
        {
            string info = string.Empty;
            info = GetHardWareInfo("Win32_BaseBoard", "SerialNumber");
            return info;
        }

        /// <summary>
        /// 返回 mac 地址，格式为 C8:60:00:02:0D:06
        /// </summary>
        /// <returns></returns>
        public static string GetMacAddress()
        {
            try
            {
                ManagementClass mc = new ManagementClass("Win32_NetworkAdapterConfiguration");
                ManagementObjectCollection moc = mc.GetInstances();
                string strMac = string.Empty;
                foreach (ManagementObject mo in moc)
                {
                    if ((bool)mo["IPEnabled"] == true)
                    {
                        strMac = mo["MacAddress"].ToString();
                    }
                }
                return strMac;
            }
            catch
            {
                return "unknown";
            }
        }

        /// <summary>
        /// 根据指定的硬件和属性，返回对应的硬件信息。
        /// </summary>
        /// <param name="typePath"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private static string GetHardWareInfo(string typePath, string key)
        {
            string info = string.Empty;
            try
            {
                ManagementClass managementClass = new ManagementClass(typePath);
                ManagementObjectCollection mn = managementClass.GetInstances();
                PropertyDataCollection properties = managementClass.Properties;


                foreach (PropertyData property in properties)
                {
                    if (property.Name == key)
                    {
                        foreach (ManagementObject m in mn)
                        {
                            info = m.Properties[property.Name].Value.ToString();
                            break;
                        }
                        break;
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return info;
        }

    }
}
