﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using WindGoes6.Data;

namespace WindGoes6.Controls
{
    /// <summary>
    /// 
    /// </summary>
    public class ControlHelper
    {
        /// <summary>
        /// 用于获取一个窗体所有标准控件的名称和键值，作用是用于生成可配置名称的文件。
        /// 用法： List<string> names = GetControlNames(Form)
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="list"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        public static List<string> GetControlNames(Control parent, List<string> list = null, string level = "")
        {
            list = new List<string>() { level + parent.Name + "=" + parent.Text.Replace("\n", "\\n") };
            level += parent.Name + ".";
            foreach (Control child in parent.Controls)
                list.AddRange(GetControlNames(child, list, level));
            return list;
        }


        public static void SetControlNames(Control parent, Configuration config, string level = "")
        {
            parent.Text = config.GetString(level + parent.Name, parent.Text);
            level += parent.Name + ".";
            foreach (Control child in parent.Controls)
                SetControlNames(child, config, level);
        }

    }
}
