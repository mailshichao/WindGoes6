﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindGoes6.Controls
{
    /// <summary>
    /// 用于向一个 TextBox 添加一个按钮。
    /// 调用格式为：
    /// TextBoxButton tbb = new TextBoxButton(textBox1);
    /// tbb.ButtonText = "测试";
    /// tbb.Click += tbb_Click;
    /// </summary>
    public class TextBoxButton
    {
        /// <summary>
        /// 鼠标进入按钮区域时的按钮背景色。
        /// </summary>
        public Color MouseEnterColor { get; set; } = Color.FromArgb(229, 241, 251);

        /// <summary>
        /// 鼠标离开按钮区域后的按钮的背景色。
        /// </summary>
        public Color MouseLeaveColor { get; set; } = Color.White;

        /// <summary>
        /// 按钮的显示名称，默认为“打开”。
        /// </summary>
        public string ButtonText
        {
            get { return label.Text; }
            set { label.Text = value; }
        }

        /// <summary>
        /// 添加的按键的点击事件。
        /// </summary>
        public event EventHandler Click;

        TextBox textBox = null;
        Label label = new Label();

        /// <summary>
        /// 初始化一个扩展按钮。
        /// </summary>
        /// <param name="textBox">所要添加按钮的文本框。</param>
        /// <param name="isDirectory">是否是打开一个文件夹。true为打开文件夹，false为打开文件。</param>
        public TextBoxButton(TextBox textBox, bool isDirectory = true)
        {
            this.textBox = textBox;
            label.BackColor = Color.White;
            label.BorderStyle = BorderStyle.FixedSingle;
            label.Text = "打开";
            label.TextAlign = ContentAlignment.MiddleCenter;
            label.Tag = textBox;
            label.Name = isDirectory ? "directory" : "file";
            label.Click += new System.EventHandler(LblOPen_Click);
            label.MouseEnter += new System.EventHandler(LblOPen_MouseEnter);
            label.MouseLeave += new System.EventHandler(LblOPen_MouseLeave);
            label.Size = new Size(50, textBox.Height);
            label.Location = new Point(textBox.Left + textBox.Width - label.Width, textBox.Top);
            textBox.Parent.Controls.Add(label);
            label.BringToFront();
            textBox.Width -= label.Width - 1;
            textBox.SizeChanged += textBox_SizeChanged;
        }

        private void textBox_SizeChanged(object sender, EventArgs e)
        {
            label.Size = new Size(50, textBox.Height);
            label.Location = new Point(textBox.Left + textBox.Width - label.Width, textBox.Top);
        }

        void LblOPen_MouseLeave(object sender, EventArgs e)
        {
            label.BackColor = MouseLeaveColor;
        }

        void LblOPen_MouseEnter(object sender, EventArgs e)
        {
            label.BackColor = MouseEnterColor;
        }

        void LblOPen_Click(object sender, EventArgs e)
        {
            if (Click != null)
            {
                Click.Invoke(sender, e);
                return;
            }
            else if (label.Name == "directory")
            {
                FolderBrowserDialog fbd = new FolderBrowserDialog();
                fbd.SelectedPath = textBox.Text;
                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    textBox.Text = fbd.SelectedPath;
                }
            }
            else if (label.Name == "file")
            {
                OpenFileDialog ofd = new OpenFileDialog();
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    textBox.Text = ofd.FileName;
                }
            }
        }
    }
}
