﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindGoes6.Data
{
    /// <summary>
    /// 表示[Start, End) 的范围。
    /// </summary>
    class IntRange
    {
        public int Start { get; set; }
        public int End { get; set; }
    }
}
