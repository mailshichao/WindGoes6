﻿/*
 * 【作者】郝伟老师
 * 【使用】日志类型类。
 * 【说明】 
 *  日志的类型信息，数据类型为整型，可根据需要扩展。
 * 
 * 【历史更新记录】
 *  2019/06/14   V1.0    初步建立此类。 
 * 
 * 
 */


namespace WindGoes6.Data.Logs
{
    /// <summary>
    /// Used to represent different types of logs based on Integers.
    /// 1: System Running Info
    /// 2: System Errors
    /// 3: Debug
    /// </summary>
    public class LogType 
    {
        /// <summary>
        /// System Information, such as CPU usage, storage, network etc.
        /// </summary>
        public readonly static int SystemInfo_1001 = 1001;
        /// <summary>
        /// Exceptions, such as IOException, OutofIndexException, etc.
        /// </summary>
        public readonly static int Exception_2001 = 2001;
        /// <summary>
        /// Used for debug, such as variables, method calls, etc.
        /// </summary>
        public readonly static int Debug_3001 = 3001;
        /// <summary>
        /// Used for possible errors, such as too small, overflow, etc.
        /// </summary>
        public readonly static int Error_4001 = 4001;

    }
}
