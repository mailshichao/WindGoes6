﻿/*
 * 名称：连接字符串自动生成类
 * 
 * 作用：根据传入参数生成连接字符串。
 *
 * 更新：
 *       2010-3-21  初步建立这个类。
 *       2011-6-3   添加向Ini文件读写的方法。
 *       2011-8-7   添加TestConnection方法。
 *       2020/06/28 重写些类，把多余的内容全部删除。
 * 
 */

using System.Data.SqlClient;

namespace WindGoes6.Data.SqlServer
{
	/// <summary>
	/// Connections 的摘要说明
	/// </summary>
	public class SQLConnection
	{
		#region 变量和属性 
		/// <summary>
		/// 数据源
		/// </summary>
		public string Server { get; set; }

		/// <summary>
		/// 初始化数据库
		/// </summary>
		public string Database { get; set; }

		/// <summary>
		/// 用户帐号
		/// </summary>
		public string UserID { get; set; }

		/// <summary>
		/// 用户密码。
		/// </summary>
		public string Password { get; set; }

		/// <summary>
		/// 是否为本地连接，true表示本地，false表示远程连接。
		/// </summary>
		public bool Trusted{ get; set; }

		/// <summary>
		/// 最终生成的数据库连接字符串。
		/// </summary>
		public string ConnectionString
		{
			get
			{
				string con = string.Format("Data Source = {0};Initial Catalog = {1};", Server, Database);
				return Trusted ? con + "Trusted_Connection = true;" : con + string.Format("User ID = {0}; Password = {1};", UserID, Password);
			}
		}

		#endregion

		/// <summary>
		/// 用于生成连接字符串的类。
		/// </summary>
		/// <param name="server">需要用于连接的服务器的名称</param>
		/// <param name="database">设置需要连接的数据库名称</param>
		public SQLConnection(string server, string database)
		{
			Server = server;
			Database = database;
			Trusted = true;
		}

		/// <summary>
		/// 用于生成连接字符串的类。
		/// </summary>
		public SQLConnection() { }

		/// <summary>
		/// 用于生成连接字符串的类。
		/// </summary>
		/// <param name="server">需要用于连接的服务器的名称</param>
		/// <param name="database">设置需要连接的数据库名称</param>
		/// <param name="userID">连接数据库所需要的帐号</param>
		/// <param name="password">连接数据库所需要的帐号的密码。</param>
		public SQLConnection(string server, string database, string userID, string password)
		{
			Server = server;
			Database = database;
			UserID = userID;
			Password = password;
			Trusted = false;
		}

		/// <summary>
		/// 测试当前连接是否可以连接成功，由于加入了超时3秒，所以最多卡 timeout 秒。
		/// </summary>
		/// <param name="sqlcon"></param>
		/// <param name="timeout"></param>
		/// <returns></returns>
		public static bool TestConnection(SQLConnection sqlcon, int timeout = 3)
		{
			bool successsful = false;
			try
			{
				SqlConnection sql = new SqlConnection($"{sqlcon.ConnectionString};Connect Timeout={timeout};");
				sql.Open();
				sql.Close();
				successsful = true;
			}
			catch { }
			return successsful;
		} 
	}
}
