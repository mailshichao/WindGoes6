﻿/*
 * 名称：连接字符串自动生成类
 * 
 * 作用：根据传入参数生成连接字符串。
 *
 * 
 * 时间：2010-3-21  初步建立这个类。
 *       2011-6-3   添加向Ini文件读写的方法。
 *       2011-8-7   添加TestConnection方法。
 * 更新：无
 * 
 * 
 */

using System;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.IO;
using System.Windows.Forms;
using WindGoes6.Security;

namespace WindGoes6.Database
{
    /// <summary>
    /// Connections 的摘要说明
    /// </summary>
    public class SQLConnection
    {
        #region 变量和属性 
        /// <summary>
        /// 数据源
        /// </summary>
        public string DataSource { get; set; }

        /// <summary>
        /// 初始化数据库
        /// </summary>
        public string InitialCatalog { get; set; }

        /// <summary>
        /// 用户帐号
        /// </summary>
        public string UserID { get; set; }

        /// <summary>
        /// 用户密码。
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 是否为本地连接，true表示本地，false表示远程连接。
        /// </summary>
        public bool TrustedConnection { get; set; }

        /// <summary>
        /// 最终生成的数据库连接字符串。
        /// </summary>
        public string ConnectionString
        {
            get
            {
                //如果是本地连接
                if (TrustedConnection)
                    return string.Format("Data Source = {0};Initial Catalog = {1};Trusted_Connection = true;",
                        DataSource, InitialCatalog);

                //如果是远程连接
                return string.Format("Data Source = {0};Initial Catalog = {1};User ID = {2}; Password = {3};",
                    DataSource, InitialCatalog, UserID, Password);
            }
        }

        #endregion


        private void InitData(string ds, string ic, string ui, string pw, bool tc)
        {
            DataSource = ds;
            InitialCatalog = ic;
            UserID = ui;
            Password = pw;
            TrustedConnection = tc;
        }


        /// <summary>
        /// 用于生成连接字符串的类。
        /// </summary>
        /// <param name="datasource">需要用于连接的服务器的名称</param>
        /// <param name="initialcatalog">设置需要连接的数据库名称</param>
        public SQLConnection(string datasource, string initialcatalog)
        {
            InitData(datasource, initialcatalog, "", "", true);
        }

        /// <summary>
        /// 用于生成连接字符串的类。
        /// </summary>
        public SQLConnection() { }

        /// <summary>
        /// 用于生成连接字符串的类。
        /// </summary>
        /// <param name="dataSource">需要用于连接的服务器的名称</param>
        /// <param name="initialDatabase">设置需要连接的数据库名称</param>
        /// <param name="userID">连接数据库所需要的帐号</param>
        /// <param name="password">连接数据库所需要的帐号的密码。</param>
        public SQLConnection(string dataSource, string initialDatabase, string userID, string password)
        {
            InitData(dataSource, initialDatabase, userID, password, false);
        }

        /// <summary>
        /// 测试当前连接是否可以连接成功，由于加入了超时3秒，所以最多卡 timeout 秒。
        /// </summary>
        /// <param name="timeout"></param>
        /// <returns></returns>
		public bool TestConnection(int timeout = 3)
        {
            bool successsful = false;
            try
            {
                string sqlcon = $"{ConnectionString};Connect Timeout={timeout};";
                SqlConnection sql = new SqlConnection(sqlcon);
                sql.Open();
                sql.Close();
                successsful = true;
            }
            catch { }
            return successsful;
        }

        /// <summary>
        /// 从字符串初始化对象。
        /// </summary>
        /// <param name="s">输入字符串</param>
        /// <param name="des">是否使用DES加密。</param>
        /// <returns></returns>
        public static SQLConnection FromDesString(string s, bool des)
        {
            SQLConnection cn = new SQLConnection();
            try
            {
                s = des ? DESCrypto.Decrypt(s) : s;
                string[] data = s.Split(',');
                cn.DataSource = data[0];
                cn.InitialCatalog = data[1];
                cn.UserID = data[2];
                cn.Password = data[3];
                cn.TrustedConnection = bool.Parse(data[4]);
            }
            catch { cn = null; }


            return cn;
        }


        /// <summary>
        /// 将字符串转为DES加密的字符串
        /// </summary>
        /// <param name="des">是否使用DES</param>
        /// <returns></returns>
        public string ToDESString(bool des)
        {
            string s =  $"{DataSource},{InitialCatalog},{UserID},{Password},{TrustedConnection}";
            //des为真则加密。 
            return des ? DESCrypto.Encrypt(s) : s;
        }
    }
}
