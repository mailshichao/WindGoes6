/****************************************
 * Desc:	BalloonTip
 * Author:	hoodlum1980
 * Email:	jinfd@126.com
 * Blog:	http://www.cnblogs.com/hoodlum1980
 * Date:	2008.05.10 19：00
 ****************************************/
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Reflection;
using System.Runtime.InteropServices;//for platform invoke

namespace WindGoes6.Forms
{
	/// <summary>
	/// 图标类型
	/// </summary>
	public enum BalloonTipIconType : int
	{
        /// <summary>
        /// 空图标
        /// </summary>
		None = 0,
        /// <summary>
        /// 错误图标
        /// </summary>
		Error = 1,
        /// <summary>
        /// 问号图标
        /// </summary>
		Question = 2,
        /// <summary>
        /// 警告图标
        /// </summary>
		Warning = 3,
        /// <summary>
        /// 信息图标
        /// </summary>
		Information = 4,
        /// <summary>
        /// 用户指定图标
        /// </summary>
		UserDefined=100,
	}

	/// <summary>
	/// BalloonTip : 
	/// 模仿TrayIcon上弹出的气泡提示，但不局限于TrayIcon，可以在任意位置弹出
	/// </summary>
	public class BalloonTip : System.Windows.Forms.Form
	{
		#region API - MESSAGES DEFINE
		private const int WM_MOUSEFIRST       = 0x0200;
		private const int WM_MOUSEMOVE        = 0x0200;
		private const int WM_LBUTTONDOWN      = 0x0201;
		private const int WM_LBUTTONUP        = 0x0202;
		private const int WM_LBUTTONDBLCLK    = 0x0203;
		private const int WM_RBUTTONDOWN      = 0x0204;
		private const int WM_RBUTTONUP        = 0x0205;
		private const int WM_RBUTTONDBLCLK    = 0x0206;
		private const int WM_MBUTTONDOWN      = 0x0207;
		private const int WM_MBUTTONUP        = 0x0208;
		private const int WM_MBUTTONDBLCLK    = 0x0209;

		/*
		 * Key State Masks for Mouse Messages
		 */
		private const int MK_LBUTTON          = 0x0001;
		private const int MK_RBUTTON          = 0x0002;
		private const int MK_SHIFT            = 0x0004;
		private const int MK_CONTROL          = 0x0008;
		private const int MK_MBUTTON          = 0x0010;
		private const int MK_XBUTTON1         = 0x0020;
		private const int MK_XBUTTON2         = 0x0040;
		/*
		* ShowWindow() Commands
		*/
//		private const int SW_HIDE             = 0;
//		private const int SW_SHOWNORMAL       = 1;
//		private const int SW_NORMAL           = 1;
//		private const int SW_SHOWMINIMIZED    = 2;
//		private const int SW_SHOWMAXIMIZED    = 3;
//		private const int SW_MAXIMIZE         = 3;
		private const int SW_SHOWNOACTIVATE   = 4;
//		private const int SW_SHOW             = 5;
//		private const int SW_MINIMIZE         = 6;
//		private const int SW_SHOWMINNOACTIVE  = 7;
//		private const int SW_SHOWNA           = 8;
//		private const int SW_RESTORE          = 9;
//		private const int SW_SHOWDEFAULT      = 10;
//		private const int SW_FORCEMINIMIZE    = 11;
//		private const int SW_MAX              = 11;
		#endregion

		#region Platform Invoke
		[DllImport("user32")]
		private static extern IntPtr GetForegroundWindow();
		[DllImport("user32")]
		private static extern IntPtr SetForegroundWindow(IntPtr hWnd);
		[DllImport("user32")]
		private static extern bool ShowWindow(IntPtr hWnd,int nCmdShow);
		#endregion

		#region 自定义变量
		/// <summary>
		/// 当前被悬停对象枚举
		/// </summary>
		private enum HoverComponent
		{
			None,
			BalloonTip,
			CloseButton
		};
		//当前被悬停对象
		private HoverComponent m_CurHover=HoverComponent.None;

		//绘制在标题左侧的小图标
		private Icon m_Icon=null;
		private BalloonTipIconType m_IconType=BalloonTipIconType.None;
		//关闭按钮
		private Bitmap m_CloseButtonBitmap = null;
		private bool b_ShowCloseButton=true;
		//标题
		private string m_Title;
		//内容文本
		private string m_Content;
		//GDI Objects
		private SolidBrush m_Brush;
		private Pen m_Pen;
		//colors setting
		private Color m_ColorBackground = Color.LightYellow;
		private Color m_ColorBorder = Color.Gray;
		private Color m_ColorText = Color.Black;
		//气泡的轮廓
		private GraphicsPath m_Path;//路径！
		private Region m_Region;//气泡绘制区域
		//fonts
		private Font m_FontTitle=new Font("宋体",9,FontStyle.Bold);
		private Font m_FontContent=new Font("宋体",9,FontStyle.Regular);
		//string format
		private StringFormat m_FormatTitle=new StringFormat();
		private StringFormat m_FormatContent=new StringFormat();
		//定时器间隔
		private int m_TimerInterval=8000;

		private bool b_MouseOverCloseButton = false;
		private bool b_MouseDown = false;


		//几个主要的Bounds（Banner）
		private Rectangle m_RcClose;
		private Rectangle m_RcIcon;
		private Rectangle m_RcTitle;
		private Rectangle m_RcContent;
		//影响尺寸和内容布局的几个主要参数
		private int r=7;//圆角矩形的半径
		private int gap=4;//相邻Rect的缝隙宽度
		private int extend=12;//角型箭头 向下伸展的距离
		private int m_MaxContentWidth=500;

		//当前的焦点(箭头指向的点，屏幕坐标）
		private int m_FocusX=-1;
		private int m_FocusY=-1;

		//exposed events
		public event EventHandler BalloonClick = null;
		
		#endregion

		private System.Windows.Forms.Timer m_Timer;
		private System.ComponentModel.IContainer components;

		#region 外部属性接口
		public string Title
		{
			get
			{
				return this.m_Title;
			}
			set
			{
				if(this.m_Title!=value)
				{
					this.m_Title=value;
					if(this.Visible)
					{
						this.PrepareLayout(this.m_FocusX,this.m_FocusY);
						this.Invalidate();
					}
				}
			}
		}

		public string Content
		{
			get
			{
				return this.m_Content;
			}
			set
			{
				if(this.m_Content!=value)
				{
					this.m_Content=value;
					if(this.Visible)
					{
						this.PrepareLayout(this.m_FocusX,this.m_FocusY);
						this.Invalidate();
					}
				}
			}
		}

		public BalloonTipIconType IconType
		{
			get
			{
				return this.m_IconType;
			}
			set
			{
				if(this.m_IconType!=value)
				{
					if(value==BalloonTipIconType.None)
					{
						this.m_Icon=null;
					}
					else if(value!=BalloonTipIconType.UserDefined)
					{
						//使用资源中的一个数据流创建图片！！！
						System.Reflection.Assembly asm=Assembly.GetExecutingAssembly();
						//asm.GetName().Name="PDAUI";  资源名称为"PDAUI.NewControls.Leds.bmp"
						string filename=string.Format("{0}.images.W95MBX0{1}_2.ICO",asm.GetName().Name, (int)value);
						System.IO.Stream stream=asm.GetManifestResourceStream(filename);
						this.m_Icon=new Icon(stream,16,16);
					}
					this.m_IconType=value;
					if(this.Visible)
					{
						this.PrepareLayout(this.m_FocusX,this.m_FocusY);
						this.Invalidate();
					}
				}
			}
		}

		/// <summary>
		/// 获取或者设置用户指定的图标
		/// </summary>
		public Icon UserDefinedIcon
		{
			get
			{
				if(this.m_IconType==BalloonTipIconType.UserDefined)
					return this.m_Icon;
				else
					return null;
			}
			set
			{
				if(value!=null)
				{
					this.m_IconType=BalloonTipIconType.UserDefined;
					this.m_Icon=new Icon(value,16,16);
					
					if(this.Visible)
					{
						this.PrepareLayout(this.m_FocusX,this.m_FocusY);
						this.Invalidate();
					}
				}
			}
		}

		/// <summary>
		/// 是否显示关闭按钮
		/// </summary>
		public bool ShowCloseButton
		{
			get
			{
				return this.b_ShowCloseButton;
			}
			set
			{
				if(this.b_ShowCloseButton!=value)
				{
					this.b_ShowCloseButton=value;
					if(this.Visible)
					{
						this.PrepareLayout(this.m_FocusX,this.m_FocusY);
						this.Invalidate();
					}
				}
			}
		}

		public Color ColorBackground
		{
			get
			{
				return this.m_ColorBackground;
			}
			set
			{
				if(this.m_ColorBackground!=value)
				{
					this.m_ColorBackground=value;
					if(this.Visible)
						this.Invalidate();
				}
			}
		}

		public Color ColorBorder
		{
			get
			{
				return this.m_ColorBorder;
			}
			set
			{
				if(this.m_ColorBorder!=value)
				{
					this.m_ColorBorder=value;
					if(this.Visible)
						this.Invalidate();
				}
			}
		}

		public Color ColorText
		{
			get
			{
				return this.m_ColorText;
			}
			set
			{
				if(this.m_ColorText!=value)
				{
					this.m_ColorText=value;
					if(this.Visible)
						this.Invalidate();
				}
			}
		}

		public Font FontTitle
		{
			get
			{
				return this.m_FontTitle;
			}
			set
			{
				if(this.m_FontTitle!=value)
				{
					this.m_FontTitle=value;
					if(this.Visible)
					{
						this.PrepareLayout(this.m_FocusX,this.m_FocusY);
						this.Invalidate();
					}
				}
			}
		}

		public Font FontContent
		{
			get
			{
				return this.m_FontContent;
			}
			set
			{
				if(this.m_FontContent!=value)
				{
					this.m_FontContent=value;
					if(this.Visible)
					{
						this.PrepareLayout(this.m_FocusX,this.m_FocusY);
						this.Invalidate();
					}
				}
			}
		}

		/// <summary>
		/// 显示的时间，（秒为单位)，小于0表示不启动定时器，一直都出现
		/// </summary>
		public int TimeoutMilliseconds
		{
			get
			{
				return this.m_TimerInterval;
			}
			set
			{
                this.m_TimerInterval=value;
			}
		}

		//最大内容宽度（测量矩形时使用）
		public int MaxContentWidth
		{
			get
			{
				return this.m_MaxContentWidth;
			}
			set
			{
				if(this.m_MaxContentWidth!=value)
				{
					this.m_MaxContentWidth=value;
					if(this.Visible)
					{
						this.PrepareLayout(this.m_FocusX,this.m_FocusY);
						this.Invalidate();
					}
				}
				
			}
		}
		#endregion

		/// <summary>
		/// 构造函数
		/// </summary>
		public BalloonTip()
		{
			InitializeComponent();
			this.m_Brush=new SolidBrush(this.m_ColorBackground);
			this.m_Pen=new Pen(this.m_ColorBorder);
			this.m_FormatTitle.Alignment=StringAlignment.Center;
			this.m_FormatTitle.LineAlignment = StringAlignment.Center;
			this.m_FormatTitle.FormatFlags = StringFormatFlags.MeasureTrailingSpaces;
			this.m_FormatTitle.Trimming = StringTrimming.EllipsisCharacter;	
			this.m_FormatContent.Alignment=StringAlignment.Near;
			this.m_FormatContent.LineAlignment = StringAlignment.Near;
			this.m_FormatContent.FormatFlags = StringFormatFlags.MeasureTrailingSpaces;
			this.m_FormatContent.Trimming = StringTrimming.EllipsisCharacter;

			this.Opacity=0;
			this.PrepareLayout(0,0);
			this.Show();
			this.Hide();
		}

		/// <summary>
		/// 清理所有正在使用的资源。
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows 窗体设计器生成的代码
		/// <summary>
		/// 设计器支持所需的方法 - 不要使用代码编辑器修改
		/// 此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.m_Timer = new System.Windows.Forms.Timer(this.components);
			// 
			// m_Timer
			// 
			this.m_Timer.Interval = 15000;
			this.m_Timer.Tick += new System.EventHandler(this.m_Timer_Tick);
			// 
			// BalloonTip
			// 
			this.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
			this.BackColor = System.Drawing.Color.Magenta;
			this.CausesValidation = false;
			this.ClientSize = new System.Drawing.Size(256, 104);
			this.ControlBox = false;
			this.Enabled = false;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.ImeMode = System.Windows.Forms.ImeMode.Off;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "BalloonTip";
			this.Opacity = 0.95;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "BalloonTipForm";
			this.TopMost = true;
			this.TransparencyKey = System.Drawing.Color.Magenta;

		}
		#endregion

		#region 内部辅助方法
		/// <summary>
		/// 在显示前的准备，需要测量尺寸等
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="title"></param>
		/// <param name="text"></param>
		private void PrepareLayout(int x,int y)
		{
			//计算Title矩形
			int t=(int)(1.5*r);//下降距离右边缘的距离！
			Graphics g=this.CreateGraphics();
			SizeF sizef;
			bool bIcon=true,bTitle=true;//flag

			//设置ICON的矩形--------------------------------
			if(this.m_IconType==BalloonTipIconType.None)
			{
				this.m_RcIcon=new Rectangle(this.r,this.r,0,0);
				bIcon=false;
			}
			else
				this.m_RcIcon=new Rectangle(this.r,this.r,16,16);

			//设置Title的矩形--------------------------------
			if(this.m_Title==null ||this.m_Title.Length<=0)
			{
				this.m_RcTitle=new Rectangle(this.m_RcIcon.Right+this.gap,this.r+gap,1,1);
				bTitle=false;
			}
			else
			{
				sizef=g.MeasureString(this.m_Title,this.m_FontTitle,m_MaxContentWidth,m_FormatTitle);	
				this.m_RcTitle=new Rectangle(this.m_RcIcon.Right+this.gap,this.r,(int)(sizef.Width+1),(int)(sizef.Height+1));
			}

			//设置Content的矩形--------------------------------
			sizef=g.MeasureString(this.m_Content,this.m_FontContent,m_MaxContentWidth,m_FormatContent);
			this.m_RcContent=new Rectangle(this.m_RcIcon.Right+gap,this.m_RcTitle.Bottom + gap,(int)(sizef.Width+1),(int)(sizef.Height+1));

			//如果没有图标，或者内容在图标的下方，则将RcContent仅靠气泡左侧
			if(!bIcon || this.m_RcContent.Y>this.m_RcIcon.Bottom)
				this.m_RcContent.X=this.m_RcIcon.Left;
			if(!bTitle)
				this.m_RcContent.Y=this.m_RcTitle.Y;
			if(this.m_Content==null || this.m_Content.Length<=0)
			{
				//没有文本
				if(this.b_ShowCloseButton)
					this.m_RcContent.Height=this.m_RcClose.Height;
			}

			//设置CloseButton的矩形--------------------------------
			int right=Math.Max(this.m_RcTitle.Right,this.m_RcContent.Right);
			if(this.b_ShowCloseButton)
				this.m_RcClose=new Rectangle(right+gap,r,16,16);
			else
				this.m_RcClose=new Rectangle(right+gap,r,0,0);

			this.Size=new Size(
				this.m_RcClose.Right+r,
				Math.Max(this.m_RcClose.Bottom,this.m_RcContent.Bottom) + r + this.extend);

			g.Dispose();
			//设置窗体的位置！
			this.Location=new Point(x+t-this.Width, y-extend-r-this.m_RcContent.Bottom);

			//获取气泡的轮廓，以及其Region
			this.m_Path=new GraphicsPath();
			//大矩形的边界点
			int x2=this.Width-1;
			int y2=this.m_RcContent.Bottom+r;
			this.m_Path.StartFigure();
			this.m_Path.AddLine(r, 0,  x2-r,0);
			this.m_Path.AddArc(x2-2*r,0,  2*r,2*r,270,90);
			this.m_Path.AddLine(x2,0+r,   x2,y2-r);
			this.m_Path.AddArc(x2-2*r,y2-2*r, 2*r,2*r,0,90);

			//下边缘
			this.m_Path.AddLine(x2-r,y2,           x2-t,        y2       );
			//在这里添加向下下沉的角型箭头
			this.m_Path.AddLine(x2-t, y2,          x2-t,        y2+extend);
			this.m_Path.AddLine(x2-t, y2+extend,   x2-t-extend, y2       );
			//继续回到下边缘
			this.m_Path.AddLine(x2-t-extend,y2,    r,y2);
			this.m_Path.AddArc(0,y2-2*r,  2*r,2*r,90,90);
			this.m_Path.AddLine(0,y2-r,0,r);
			this.m_Path.AddArc(0,0,2*r,2*r,180,90);
			this.m_Path.CloseFigure();
			this.m_Region=new Region(this.m_Path);
		}

		/// <summary>
		/// 绘制关闭按钮
		/// </summary>
		/// <param name="grfx"></param>
		private void DrawCloseButton(Graphics g)
		{
			if(m_CloseButtonBitmap == null)
			{
				//加载Bitmap
				//使用资源中的一个数据流创建图片！！！
				System.Reflection.Assembly asm=Assembly.GetExecutingAssembly();
				//asm.GetName().Name="PDAUI";  资源名称为"PDAUI.NewControls.Leds.bmp"
				string filename=string.Format("{0}.images.CLOSE_BUTTON.BMP",asm.GetName().Name);
				System.IO.Stream stream=asm.GetManifestResourceStream(filename);
				this.m_CloseButtonBitmap=new Bitmap(stream);
			}

			if (m_CloseButtonBitmap != null)
			{
				int x=0;

				if (b_MouseOverCloseButton)
				{
					if (b_MouseDown) //button is pushed down by mouse
						x = m_RcClose.Width*2;
					else //button is hovered by mouse
						x = m_RcClose.Width;
				}		
				//else: button is in idle state( far away from mouse)
					
				System.Drawing.Imaging.ImageAttributes att=new System.Drawing.Imaging.ImageAttributes();
				//为图像设置透明色
				att.SetColorKey(Color.Magenta,Color.Magenta);

				g.DrawImage(m_CloseButtonBitmap, m_RcClose, 
					x,0, m_RcClose.Width,m_RcClose.Height,
					GraphicsUnit.Pixel,
					att);
			}
		}

		#endregion

		#region 公开方法

		/// <summary>
		/// 弹出窗口，指定气泡的来源点位置（屏幕点坐标）。
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="title"></param>
		/// <param name="text"></param>
		/// <param name="icon"></param>
		public void ShowAt(int x,int y,string title,string content,BalloonTipIconType icontype)
		{
			if(this.m_Timer.Enabled)
				this.m_Timer.Enabled=false;
			this.Title=title;
			this.Content=content;
			this.IconType=icontype;
			this.m_FocusX=x;
			this.m_FocusY=y;
			this.PrepareLayout(this.m_FocusX,this.m_FocusY);

			this.Opacity=100;
			//用这种方法显示窗口不会夺取当前前台窗口的焦点
			ShowWindow(this.Handle,SW_SHOWNOACTIVATE);
			if(this.m_TimerInterval>0)
			{
				this.m_Timer.Interval=this.m_TimerInterval;
				this.m_Timer.Start();
			}
		}

		/// <summary>
		/// 关闭/实际是隐藏
		/// </summary>
		public void Disappear()
		{
			this.Hide();
			//把标志清空，以免再次弹出时有错误
			this.b_MouseDown=false;
			this.b_MouseOverCloseButton=false;
		}
		#endregion

		#region override method - mouse event handler...
		protected override void OnMouseDown(MouseEventArgs e)
		{
			base.OnMouseDown(e);
			this.b_MouseDown=true;
			//this.Capture=true;
			if(this.m_RcClose.Contains(e.X,e.Y))
			{
				if(this.b_ShowCloseButton)
				{
					this.b_MouseOverCloseButton=true;
					this.Invalidate(this.m_RcClose);
					this.m_CurHover=HoverComponent.CloseButton;
				}
			}
			else
			{
				this.m_CurHover=HoverComponent.BalloonTip;
				//erase the event
				if(this.BalloonClick!=null)
					this.BalloonClick(this,EventArgs.Empty);
			}
		}

		protected override void OnMouseUp(MouseEventArgs e)
		{
			base.OnMouseUp (e);
			this.b_MouseDown=false;
			//this.Capture=false;
			//更新
			//如果鼠标是在关闭按钮上抬起的，并且跟踪对象相符，则关闭Balloon
			if(this.b_MouseOverCloseButton && this.m_CurHover==HoverComponent.CloseButton)
			{
				this.Disappear();//调用该函数取代Hide，可以清掉鼠标标记。
				//this.Invalidate(this.m_RcClose);
			}
		}

		protected override void OnMouseLeave(EventArgs e)
		{
			base.OnMouseLeave (e);
			this.m_CurHover=HoverComponent.None;
			//因为我并不想捕捉鼠标，所以确保释放掉该鼠标按下标志和悬停标志
			//这样一旦鼠标离开Balloon，所有标志都被清掉
			//而鼠标在不离开期间，对CloseButton具有标准的系统行为：
			//
			//即Down时设置跟踪对象，Up时如果与跟踪对象相符，则触发点击事件
			//在此期间如果保持按下状态移开鼠标在抬起鼠标，则由于跟踪对象不符，因此相当于
			//取消了点击操作。
			this.b_MouseDown=false;
			if(this.b_MouseOverCloseButton)
			{
				this.b_MouseOverCloseButton=false;
				this.Invalidate(this.m_RcClose);
			}
		}

		protected override void OnMouseMove(MouseEventArgs e)
		{
			base.OnMouseMove (e);
			if(this.m_RcClose.Contains(e.X,e.Y))
			{
				if(!this.b_MouseOverCloseButton)
				{
					this.b_MouseOverCloseButton=true;
					this.Invalidate(this.m_RcClose);
				}
			}
			else
			{
				if(this.b_MouseOverCloseButton)
				{
					this.b_MouseOverCloseButton=false;
					this.Invalidate(this.m_RcClose);
				}
			}
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			if(this.m_Region!=null)
			{
				this.m_Brush.Color=this.m_ColorBackground;
				e.Graphics.FillRegion(this.m_Brush,this.m_Region);
			}
			if(this.m_Path!=null)
			{
				this.m_Pen.Color=this.m_ColorBorder;
				e.Graphics.DrawPath(this.m_Pen,this.m_Path);
			}			
			if(this.m_Icon!=null)
				e.Graphics.DrawIcon(this.m_Icon,this.m_RcIcon);


			//绘制标题
			this.m_Brush.Color=this.m_ColorText;
			if(this.m_Title!=null && this.m_Title.Length>0)
			{
				e.Graphics.DrawString(this.m_Title,this.m_FontTitle,this.m_Brush,
					this.m_RcTitle,this.m_FormatTitle);
			}
			//绘制内容
			if(this.m_Content!=null)
			{
				e.Graphics.DrawString(this.m_Content,this.m_FontContent,this.m_Brush,
					this.m_RcContent,this.m_FormatContent);
			}

			//绘制CloseButton
			if(this.b_ShowCloseButton)
				this.DrawCloseButton(e.Graphics);
		}

		protected override void WndProc(ref Message m)
		{
			int pos,x,y;
			MouseEventArgs e;
			switch(m.Msg)
			{
				case WM_LBUTTONDOWN:
					pos=(int)m.LParam;
					x=pos&0xffff;
					y=pos>>16;
					e=new MouseEventArgs(MouseButtons.Left,1,x,y,0);
					this.OnMouseDown(e);
					break;
				case WM_LBUTTONUP:
					pos=(int)m.LParam;
					x=pos&0xffff;
					y=pos>>16;
					e=new MouseEventArgs(MouseButtons.Left,1,x,y,0);
					this.OnMouseUp(e);
					break;
				default:
					base.WndProc (ref m);
					break;
			}
		}

		#endregion

		/// <summary>
		/// 定时器响应函数
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void m_Timer_Tick(object sender, System.EventArgs e)
		{
			this.m_Timer.Stop();
			for(double i=this.Opacity;i>0;i-=0.01)
				this.Opacity=i;
			this.Disappear();
		}
	}
}
