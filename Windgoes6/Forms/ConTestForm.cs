﻿/*
 * 

 2019/03/20 Wind
 - 对结构进行优化
 - 修正一个不能正确存储的BUG
 
 2018-07-04  Wind
 - remove unused usings.
 - add XML descriptions.
 
 
 
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Linq;
using WindGoes6.Database;
using WindGoes6.Data;

namespace WindGoes6.Forms
{
    /// <summary>
    /// 用于数据库连接测试，及数据库连接字符字符串管理。
    /// </summary>
    public partial class ConTestForm : Form
    {
        /// <summary>
        /// 用于数据库连接测试，及数据库连接字符字符串管理。
        /// </summary>
        public ConTestForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 保存的文件路径。
        /// </summary>
        public static string FilePath { get; set; } = "connection.dat";

        /// <summary> 
        /// 连接字符串对象。
        /// </summary>
        public SQLConnection Connection { get; set; }

        /// <summary>
        /// 当前窗体是否已经连接成功。
        /// </summary>
        public bool Connected { get; set; }


        // 用于保存已经存在的数据库连接字符串信息。
        List<SQLConnection> cms = new List<SQLConnection>();

        /// 当前选择的连接
        private SQLConnection GetCurrentConnection()
        {
            return cbServer.SelectedIndex >= 0 && cbServer.SelectedIndex < cbServer.Items.Count ? cms[cbServer.SelectedIndex] : null;
        }

        // 用于记录连接时间
        DateTime startTime;

        private void ConTestForm_Load(object sender, EventArgs e)
        {
            FilePath = Path.GetFullPath(FilePath);
            if (!File.Exists(FilePath))
                return;

            // 从文件中读取所有配置信息，连接字符串采用DES加密。
            string[] constrs = File.ReadAllLines(FilePath, Encoding.Default);
            foreach (string constr in constrs)
            {
                SQLConnection sm = SQLConnection.FromDesString(constr, true);
                if (sm != null)
                {
                    cms.Add(sm);
                    cbServer.Items.Add(sm.DataSource);
                }
            }

            // 调整UI
            if (cms.Count > 0)
            {
                cbServer.SelectedIndex = 0;
                if (GetCurrentConnection().TrustedConnection)
                    rbLocalDB.Checked = true;
                else
                    rbRemoteDB.Checked = true; 
            }


            AdjustUI();
        }

        private void btnConnection_Click(object sender, EventArgs e)
        {
            AdjustUI();

            btnConnection.Enabled = false;
            startTime = DateTime.Now;
            timer1.Start();

            DBManager.ConnectionString = Connection.ConnectionString;

            MultiThreadSqlCon mt = new MultiThreadSqlCon();
            mt.ConnectionString = Connection.ConnectionString;
            mt.Timeout = (int)numericUpDown1.Value;
            mt.AfterTest += new MyEvent(mt_AfterTest);
            mt.StartTest();
        }

        void mt_AfterTest(bool result, Exception e)
        {
            btnConnection.Enabled = true;
            Connected = result;
            if (Connected)
            {
                cbDatabase.Enabled = true;
                cbDatabase.Items.Clear();
                cbDatabase.Items.AddRange(GetAllDataBase().ToArray());
            }
            timer1.Stop();

            AdjustUI();
            if (result)
            {
                MessageBox.Show("数据库连接成功!", "连接成功", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (cbDatabase.Text.Trim().Length == 0)
                    MessageBox.Show(" 请选择需要连接的数据库名称。", "数据库选择", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("数据库连接失败，原因如下：\n" + e.Message,
                    "连接失败", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private void CbServer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (GetCurrentConnection().TrustedConnection)
                rbLocalDB.Checked = true;
            else
                rbRemoteDB.Checked = true;
            AdjustUI();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            TimeSpan ts = DateTime.Now - startTime;
            lblTotalTime.Text = ts.TotalSeconds.ToString("总时间：0.0s");
            string s1 = "连接测试中";
            int t = 0;
            for (int i = 0; i < t; i++)
                s1 += ".";
            t = t > 3 ? 0 : t += 1;
            lblResult.Text = s1;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ConTestForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (timer1.Enabled)
            {
                DialogResult dr = MessageBox.Show("连接测试中，确定要退出?", "退出确认", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }

        private List<string> GetAllDataBase()
        {
            DBManager dbm = new DBManager();
            dbm.CommandText = "select name from master..sysdatabases";

            string[] data = dbm.GetColumn();

            List<string> list = new List<string>();
            for (int i = 0; i < data.Length; i++)
            {
                string name = data[i].ToLower();
                if (name == "master" || name == "tempdb" || name == "model" || name == "msdb")
                {
                    continue;
                }
                list.Add(data[i]);
                //Console.WriteLine(data[i]);
            }

            return list;
        }

        private void cbConnectionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            AdjustUI();
        }

        private void RbLocalDB_CheckedChanged(object sender, EventArgs e)
        {
            AdjustUI();
        }


        private void RbRemoteDB_CheckedChanged(object sender, EventArgs e)
        {
            AdjustUI();
        }

        private void AdjustUI()
        {
            SQLConnection con = cms.Count(c => c.DataSource == cbDatabase.Text) > 0 ? GetCurrentConnection() : Connection;


            //if (con != null && con.TrustedConnection == false)
            //    rbRemoteDB.Checked = true;
            //else
            //    rbLocalDB.Checked = true;
            txtUserName.Enabled = rbRemoteDB.Checked;
            txtPassword.Enabled = rbRemoteDB.Checked;
            string db = cbDatabase.Text.Length > 0 ? cbDatabase.Text : "master";
            Connection = rbLocalDB.Checked ?
                new SQLConnection(cbServer.Text, db) :
                new SQLConnection(cbServer.Text, db, txtUserName.Text, txtPassword.Text);
            btnSave.Enabled = con != null;
        }


        private void cbServer_TextChanged(object sender, EventArgs e)
        {
            btnConnection.Enabled = cbServer.Text.Length == 0 ? false : true;
            Connected = false;
            AdjustUI();

            for (int i = 0; i < cms.Count; i++)
            {
                if (cms[i].DataSource.ToLower().Trim() == cbServer.Text.ToLower().Trim())
                {
                    Connection = cms[i];
                    AttachUI(Connection);
                    return;
                }
            }

            Connection = null;
            AttachUI(Connection);

        }


        private void AttachUI(SQLConnection cm)
        {
            if (cm == null)
            {
                txtUserName.Clear();
                txtPassword.Clear();
                cbDatabase.Text = "";
            }
            else
            {
                rbLocalDB.Checked = cm.TrustedConnection;
                if (!cm.TrustedConnection)
                {
                    txtUserName.Text = cm.UserID;
                    txtPassword.Text = cm.Password;
                }
                cbDatabase.Text = cm.InitialCatalog;
            }
        }

        private void txtUserName_TextChanged(object sender, EventArgs e)
        {
            Connected = false;
            AdjustUI();
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            Connected = false;
            AdjustUI();
        }

        private void btGetConnectionString_Click(object sender, EventArgs e)
        {
            try
            {
                string tmpfile = Path.Combine(System.Environment.GetEnvironmentVariable("TEMP"), "connection.txt");
                using (StreamWriter sw = new StreamWriter(tmpfile, false, Encoding.Default))
                {
                    sw.WriteLine(GetCurrentConnection()?.ConnectionString);
                }
                System.Diagnostics.Process.Start("NotePad.exe", tmpfile);
                Application.DoEvents();
                Thread.Sleep(1000);
                File.Delete(tmpfile);
            }
            catch (Exception e1) { Console.WriteLine(e1.Message); }
        }

        private void cbDatabase_SelectedIndexChanged(object sender, EventArgs e)
        {
            AdjustUI();
            btnSave.Enabled = cbDatabase.SelectedIndex >= 0;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //1.若为空，表示为新连接。
            if (Connection == null)
            {
                Connection = new SQLConnection();
                Connection.DataSource = cbServer.Text.Trim();
                Connection.TrustedConnection = rbLocalDB.Checked;
                if (!Connection.TrustedConnection)
                {
                    Connection.UserID = txtUserName.Text;
                    Connection.Password = txtPassword.Text;
                }
                Connection.InitialCatalog = cbDatabase.Text;
                cms.Add(Connection);
            }
            else
            {
                Connection.DataSource = cbServer.Text.Trim();
                Connection.TrustedConnection = rbLocalDB.Checked;
                if (!Connection.TrustedConnection)
                {
                    Connection.UserID = txtUserName.Text;
                    Connection.Password = txtPassword.Text;
                }
                Connection.InitialCatalog = cbDatabase.Text;
            }

            //2.移除原来队伍中已经存在的conManager
            for (int i = 0; i < cms.Count; i++)
            {
                if (cms[i].DataSource == Connection.DataSource)
                {
                    cms.RemoveAt(i);
                    break;
                }
            }

            //3.写文件，其中conManager先写入，方便下次读取时自动关联。
            using (StreamWriter sw = new StreamWriter(FilePath, false, Encoding.Default))
            {
                sw.WriteLine(Connection.ToDESString(true));
                for (int i = 0; i < cms.Count; i++)
                    sw.WriteLine(cms[i].ToDESString(true));
            }

            //4.关闭窗体
            Close();
        }






        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbRemoteDB = new System.Windows.Forms.RadioButton();
            this.rbLocalDB = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbDatabase = new System.Windows.Forms.ComboBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.cbServer = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnConnection = new System.Windows.Forms.Button();
            this.lblTotalTime = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.lblResult = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btGetConnectionString = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbRemoteDB);
            this.groupBox2.Controls.Add(this.rbLocalDB);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.cbDatabase);
            this.groupBox2.Controls.Add(this.txtPassword);
            this.groupBox2.Controls.Add(this.cbServer);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtUserName);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(10, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(499, 152);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "服务器连接配置";
            // 
            // rbRemoteDB
            // 
            this.rbRemoteDB.AutoSize = true;
            this.rbRemoteDB.Location = new System.Drawing.Point(310, 19);
            this.rbRemoteDB.Name = "rbRemoteDB";
            this.rbRemoteDB.Size = new System.Drawing.Size(107, 16);
            this.rbRemoteDB.TabIndex = 21;
            this.rbRemoteDB.Text = "远程数据库连接";
            this.rbRemoteDB.UseVisualStyleBackColor = true;
            this.rbRemoteDB.CheckedChanged += new System.EventHandler(this.RbRemoteDB_CheckedChanged);
            // 
            // rbLocalDB
            // 
            this.rbLocalDB.AutoSize = true;
            this.rbLocalDB.Checked = true;
            this.rbLocalDB.Location = new System.Drawing.Point(139, 19);
            this.rbLocalDB.Name = "rbLocalDB";
            this.rbLocalDB.Size = new System.Drawing.Size(107, 16);
            this.rbLocalDB.TabIndex = 20;
            this.rbLocalDB.TabStop = true;
            this.rbLocalDB.Text = "本地数据库连接";
            this.rbLocalDB.UseVisualStyleBackColor = true;
            this.rbLocalDB.CheckedChanged += new System.EventHandler(this.RbLocalDB_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 120);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 19;
            this.label7.Text = "连接数据库";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 23);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 17;
            this.label6.Text = "连接方式";
            // 
            // cbDatabase
            // 
            this.cbDatabase.FormattingEnabled = true;
            this.cbDatabase.ItemHeight = 12;
            this.cbDatabase.Location = new System.Drawing.Point(139, 117);
            this.cbDatabase.Margin = new System.Windows.Forms.Padding(2);
            this.cbDatabase.Name = "cbDatabase";
            this.cbDatabase.Size = new System.Drawing.Size(341, 20);
            this.cbDatabase.TabIndex = 18;
            this.cbDatabase.SelectedIndexChanged += new System.EventHandler(this.cbDatabase_SelectedIndexChanged);
            // 
            // txtPassword
            // 
            this.txtPassword.Enabled = false;
            this.txtPassword.Location = new System.Drawing.Point(139, 92);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(2);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(341, 21);
            this.txtPassword.TabIndex = 4;
            this.txtPassword.TextChanged += new System.EventHandler(this.txtPassword_TextChanged);
            // 
            // cbServer
            // 
            this.cbServer.FormattingEnabled = true;
            this.cbServer.Location = new System.Drawing.Point(139, 43);
            this.cbServer.Margin = new System.Windows.Forms.Padding(2);
            this.cbServer.Name = "cbServer";
            this.cbServer.Size = new System.Drawing.Size(341, 20);
            this.cbServer.TabIndex = 1;
            this.cbServer.Text = "DESKTOP-DTPQL14";
            this.cbServer.SelectedIndexChanged += new System.EventHandler(this.CbServer_SelectedIndexChanged);
            this.cbServer.TextChanged += new System.EventHandler(this.cbServer_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(76, 95);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "密  码";
            // 
            // txtUserName
            // 
            this.txtUserName.Enabled = false;
            this.txtUserName.Location = new System.Drawing.Point(139, 67);
            this.txtUserName.Margin = new System.Windows.Forms.Padding(2);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(341, 21);
            this.txtUserName.TabIndex = 3;
            this.txtUserName.TextChanged += new System.EventHandler(this.txtUserName_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(76, 70);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "用户名";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 45);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "服务器名";
            // 
            // btnConnection
            // 
            this.btnConnection.Location = new System.Drawing.Point(403, 60);
            this.btnConnection.Margin = new System.Windows.Forms.Padding(2);
            this.btnConnection.Name = "btnConnection";
            this.btnConnection.Size = new System.Drawing.Size(76, 28);
            this.btnConnection.TabIndex = 11;
            this.btnConnection.Text = "连接测试";
            this.btnConnection.UseVisualStyleBackColor = true;
            this.btnConnection.Click += new System.EventHandler(this.btnConnection_Click);
            // 
            // lblTotalTime
            // 
            this.lblTotalTime.AutoSize = true;
            this.lblTotalTime.Location = new System.Drawing.Point(414, 28);
            this.lblTotalTime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTotalTime.Name = "lblTotalTime";
            this.lblTotalTime.Size = new System.Drawing.Size(65, 12);
            this.lblTotalTime.TabIndex = 15;
            this.lblTotalTime.Text = "已用时：0s";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(138, 19);
            this.numericUpDown1.Margin = new System.Windows.Forms.Padding(2);
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(50, 21);
            this.numericUpDown1.TabIndex = 20;
            this.numericUpDown1.TabStop = false;
            this.numericUpDown1.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(48, 23);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 13;
            this.label5.Text = "测试时间";
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.BackColor = System.Drawing.Color.Transparent;
            this.lblResult.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblResult.Location = new System.Drawing.Point(135, 62);
            this.lblResult.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(49, 14);
            this.lblResult.TabIndex = 12;
            this.lblResult.Text = "未测试";
            this.lblResult.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(414, 276);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(76, 28);
            this.btnSave.TabIndex = 14;
            this.btnSave.Text = "保存退出";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(311, 276);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(76, 28);
            this.btnClose.TabIndex = 13;
            this.btnClose.Text = "取消";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btGetConnectionString
            // 
            this.btGetConnectionString.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btGetConnectionString.Location = new System.Drawing.Point(11, 276);
            this.btGetConnectionString.Margin = new System.Windows.Forms.Padding(2);
            this.btGetConnectionString.Name = "btGetConnectionString";
            this.btGetConnectionString.Size = new System.Drawing.Size(101, 28);
            this.btGetConnectionString.TabIndex = 12;
            this.btGetConnectionString.Text = "查看连接字符串";
            this.btGetConnectionString.UseVisualStyleBackColor = true;
            this.btGetConnectionString.Click += new System.EventHandler(this.btGetConnectionString_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(192, 23);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 12);
            this.label2.TabIndex = 21;
            this.label2.Text = "秒";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.btnConnection);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.numericUpDown1);
            this.groupBox1.Controls.Add(this.lblResult);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.lblTotalTime);
            this.groupBox1.Location = new System.Drawing.Point(11, 177);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(497, 94);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(47, 60);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 14);
            this.label8.TabIndex = 22;
            this.label8.Text = "测试结果";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ConTestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(521, 317);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btGetConnectionString);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.groupBox2);
            this.Font = new System.Drawing.Font("宋体", 9F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConTestForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sql Service 数据库连接测试";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ConTestForm_FormClosing);
            this.Load += new System.EventHandler(this.ConTestForm_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code



        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnConnection;
        private System.Windows.Forms.Label lblTotalTime;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ComboBox cbServer;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbDatabase;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btGetConnectionString;
        private System.Windows.Forms.Label label2;
        private GroupBox groupBox1;
        private Label label8;
        private RadioButton rbRemoteDB;
        private RadioButton rbLocalDB;

    }
}
