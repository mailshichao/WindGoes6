﻿/*
 * 【名称】窗体辅助类。
 * 【作者】郝伟
 * 【说明】 
 *  用于提供一些窗体的辅助功能。
 * 
 * 
 * 【历史更新记录】
 * 2019/07/16   V1.0    初步建立此类。 
 * 
 * 
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindGoes6.Forms
{
    /// <summary>
    /// 用于窗体的一些辅助功能。
    /// </summary>
    public class FormHelper
    {
        /// <summary>
        /// 向窗体控件添加Bolloon提示。
        /// </summary>
        /// <param name="control"></param>
        /// <param name="title"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public static BalloonTip AddBoollonTip(Control control, string title = "标题", string content = "内容")
        {
            BalloonTip bolloonform = new BalloonTip();
            bolloonform.TimeoutMilliseconds = -1;
            control.MouseDown += (sender, e) =>
            {
                Point pos = control.PointToScreen(new Point(0, 0));
                bolloonform.ShowAt(pos.X, pos.Y, title, content, BalloonTipIconType.Information);
                return;
            };

            control.MouseUp += (sender, e) =>
            {
                bolloonform.Hide();
            };

            return bolloonform;
        }
    }
}
