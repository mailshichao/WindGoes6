﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindGoes6.Helper
{
    /// <summary>
    /// 此类包括了一些用于处理中文字符的方法。
    /// </summary>
    public class ChineseHelper
    {
        /// <summary>
        /// 从指定的字符串中抽取出所有的汉字（注：不包括标点）。
        /// 如，输入为“你好，中国。This is 我的国家。”，可得输出“你好中国我的国家”。
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string AbstractChinese(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if (c >= '\u4e00' && c <= '\u9fa5')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        /// <summary>
        /// Abstract all Chinese punctuations from the input string.
        /// Example: “你好，中国。This is 我的国家。”-->“，。”。
        /// 附所有标点：
        /// ＀  ！  ＂  ＃  ＄  ％  ＆  ＇  （  ）  ＊  ＋  ，  －  ．  ／  
        /// ０  １  ２  ３  ４  ５  ６  ７  ８  ９  ：  ；  ＜  ＝  ＞  ？  
        /// ＠  Ａ Ｂ  Ｃ Ｄ  Ｅ Ｆ  Ｇ Ｈ  Ｉ Ｊ  Ｋ Ｌ  Ｍ Ｎ  Ｏ
        /// Ｐ  Ｑ Ｒ  Ｓ Ｔ  Ｕ Ｖ  Ｗ Ｘ  Ｙ Ｚ  ［  ＼  ］  ＾  ＿  
        /// ａ ｂ  ｃ ｄ  ｅ ｆ  ｇ ｈ  ｉ ｊ  ｋ ｌ  ｍ ｎ  ｏ
        /// ｐ  ｑ ｒ  ｓ ｔ  ｕ ｖ  ｗ ｘ  ｙ ｚ  ｛  ｜  ｝  ～  ｟  
        /// ｠  ｡  ｢  ｣  
        /// </summary>
        /// <param name="s"></param>
        public static string AbstractPunctuation(string str)
        {
            
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if (c >= '\u4e00' && c <= '\u9fa5')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

    }
}
