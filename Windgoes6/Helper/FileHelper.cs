﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindGoes6.Helper
{
    /// <summary>
    /// 提供了一些简化文件操作的类。
    /// </summary>
    public class FileHelper
    {
        /// <summary>
        /// 从指定的文件中读取一段数据。如果指定区域与文件无重叠，返回为null，如 文件长度为100，要求 [200,300]的数据；
        /// 如果指定区域完全或部分重叠则返回重叠的部分，如 长度为100，要求 [50,200]，则返回 [50,100]的数据。
        /// </summary>
        /// <param name="filepath">文件路径。</param>
        /// <param name="start">开始文件位置。</param>
        /// <param name="length">需求的文件长度。</param>
        /// <returns></returns>
        public static byte[] ReadFilePart(string filepath, int start, int length)
        {
            // 定义文件并判断文件的可用性。
            FileInfo file = new FileInfo(filepath); 
            if (!file.Exists || file.Length < start)
                return null;
 
            // 需求的数据。
            byte[] bts;
            // 读取指定区域的数据。
            using (FileStream fs = new FileStream(filepath, FileMode.Open, FileAccess.Read))
            {
                fs.Seek(start, SeekOrigin.Begin);
                int end = start + length >= fs.Length ? (int)fs.Length : start + length;
                bts = new byte[end - start];
                fs.Read(bts, start, bts.Length);
            }

            return bts;
        }
    }
}
