﻿/*
 * 2018-12-10 创建 
 * 
 */

using System.Management;

namespace WindGoes6.Helper
{
	/// <summary>
	/// 硬件信息获取类。
	/// </summary>
	public class HardwareHelper
	{
		/// <summary>
		/// 返回本机的MAC地址。
		/// </summary>
		/// <returns></returns>
		public static string GetMacAddress()
		{
			try
			{
				string strMac = string.Empty;
				ManagementClass mc = new ManagementClass("Win32_NetworkAdapterConfiguration");
				ManagementObjectCollection moc = mc.GetInstances();
				foreach (ManagementObject mo in moc)
				{
					if ((bool)mo["IPEnabled"] == true)
					{
						strMac = mo["MacAddress"].ToString();
					}
				}
				moc = null;
				mc = null;
				return strMac;
			}
			catch
			{
				return "unknown";
			}
		}
	}
}
