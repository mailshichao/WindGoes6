﻿using System.IO;

namespace WindGoes6.IO
{
	/// <summary>
	/// 将一个完全的文件路径如 c:\data\file1.xml 进行拆分，方便路径管理时的操作。
	/// 必需通过 FilenameInfo.Parse(filepath); 进行初始化。初始化成功以后，所有的属性为只读；
	/// 如果初始化失败，则返回为null。
	/// </summary>
	public class FilenameInfo
	{
		/// <summary>
		/// 文件全路径，包括文件名，如：c:\\data\\file1.xml 。
		/// </summary>
		public string Fullpath { get; set; }

		/// <summary>
		/// 文件的名子，如c:\data\file1.xml的名子为 file1.xml。
		/// </summary>
		public string Filename { get; private set; }

		/// <summary>
		/// 文件的名子，不包括扩展名，如 c:\data\file1.xml 的名子为 file1。
		/// </summary>
		public string FilenameNoext { get; private set; }

		/// <summary>
		/// 扩展名，如 c:\data\file1.xml 的扩展名为 .xml。
		/// </summary>
		public string Extention { get; private set; }

		/// <summary>
		/// 目录名称，如 c:\data 的目录名为 c:\data
		/// </summary>
		public string Directory { get; private set; }

		/// <summary>
		/// 每一级目录的名称, 由根至叶分别为自左各右，如 c:\\data\\file1.xml 表示为 { "C", "data"}。
		/// </summary>
		public string[] SubDirectoryNames { get; private set; }

		/// <summary>
		/// 目录的深度，如 C:\abc 返回为2.
		/// </summary>
		public int Depth { get { return SubDirectoryNames.Length; } }

		private FilenameInfo() { }

		/// <summary>
		/// 通过本函数进行数据初始化。
		/// </summary>
		/// <param name="filePath">转入文件的完整路径，如c:\data\file1.xml。</param>
		/// <returns></returns>
		public static FilenameInfo Parse(string filePath)
		{
			if (!File.Exists(filePath))
				return null;

			FilenameInfo fi = new FilenameInfo
			{
				Fullpath = filePath,
				Filename = Path.GetFileName(filePath),
				FilenameNoext = Path.GetFileNameWithoutExtension(filePath),
				Extention = Path.GetExtension(filePath),
				Directory = Path.GetDirectoryName(filePath)
			};
			fi.SubDirectoryNames = fi.Directory.Split('\\');

			return fi;
		}
	}
}
