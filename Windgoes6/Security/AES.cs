﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace WindGoes6.Security
{
    /// <summary>
    /// 本函数使用AES方法提供了两对函数，一对 ENcrypt 和 Decrypt 不使用私钥。
    /// 另一对 AesENcrypt 和 AesDecrypt 使用私钥。 
    ///  </summary>
    public class AES
    {
        /// <summary>
        /// 使用AES加密。 
        /// </summary>
        /// <param name="toBeEncrypted">待加密字符串。</param>
        /// <param name="key">密钥。</param> 
        /// <returns></returns>
        public static string Encrypt(string toBeEncrypted, string key)
        {
            byte[] keyArray = UTF8Encoding.UTF8.GetBytes(key);
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toBeEncrypted);

            RijndaelManaged rDel = new RijndaelManaged();
            rDel.Key = keyArray;
            rDel.Mode = CipherMode.ECB;
            rDel.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = rDel.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }


        /// <summary>
        /// 使用AES进行解密。
        /// </summary>
        /// <param name="toBeDecrypted"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string Decrypt(string toBeDecrypted, string key)
        {
            byte[] keyArray = UTF8Encoding.UTF8.GetBytes(key);
            byte[] toEncryptArray = Convert.FromBase64String(toBeDecrypted);

            RijndaelManaged rDel = new RijndaelManaged();
            rDel.Key = keyArray;
            rDel.Mode = CipherMode.ECB;
            rDel.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = rDel.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return UTF8Encoding.UTF8.GetString(resultArray);
        }
         

        /// <summary>
        /// 使用公钥和密钥对字符串进行AES加密，加密模型为CBC，Padding为PKS7。
        /// 密钥使用UTF8编码，输出字符串使用Base64编码。
        /// 为了保证URL可用，将 '\\' -> '_' 和 '+' -> '-'。
        /// 如果因输入有误等导致错误，输出为 null。
        /// </summary>
        /// <param name="text">待加密文本。</param>
        /// <param name="publiKey">公钥。</param>
        /// <param name="privateKey">密钥。</param>
        /// <returns></returns>
        public static string AESEncrypt(string text, string publiKey, string privateKey = null)
        {
            string result = null;
            try
            {
                RijndaelManaged aes = new RijndaelManaged(); 
                aes.IV = Encoding.UTF8.GetBytes((privateKey+ "1981071319841228").Substring(0,16));
                aes.Key = Encoding.UTF8.GetBytes(publiKey);
                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.PKCS7;
                byte[] valuebts = Encoding.UTF8.GetBytes(text);
                byte[] encyptbts = aes.CreateEncryptor().TransformFinalBlock(valuebts, 0, valuebts.Length);
                result = Convert.ToBase64String(encyptbts, 0, encyptbts.Length);
                result = result.Replace('\\', '_').Replace('+', '-');
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return result;
        }

        /// <summary>
        /// 使用公钥和密钥对字符串进行AES解密，加密模型为CBC，Padding为PKS7。
        /// 密钥使用UTF8编码，输出字符串使用Base64编码。
        /// 为了保证URL可用，将 '_' -> '\\' 和 '-' -> '+'。
        /// 如果输入有误或解密失败，输出为 null。
        /// </summary>
        /// <param name="text">待解密文本。</param>
        /// <param name="publiKey">公钥。</param>
        /// <param name="privateKey">密钥。</param>
        /// <returns></returns>
        public static string AESDecrypt(string value, string publicKey, string privateKey = null)
        {
            string decrypted = null;
            try
            {
                RijndaelManaged aes = new RijndaelManaged();
                aes.IV = Encoding.UTF8.GetBytes((privateKey + "1981071319841228").Substring(0, 16));
                aes.Key = Encoding.UTF8.GetBytes(publicKey);
                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.PKCS7;
                byte[] bts = Convert.FromBase64String(value.Replace('_', '\\').Replace('-', '+')); 
                decrypted = Encoding.UTF8.GetString(aes.CreateDecryptor().TransformFinalBlock(bts, 0, bts.Length));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return decrypted;
        }

    }

}
