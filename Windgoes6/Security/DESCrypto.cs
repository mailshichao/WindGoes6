﻿/*
 * 名称：用于字符串数据的加密或解密。
 * 
 * 作用：主要包括加密Encrypt(string)和解密Decrypt(string)2个函数，通过InitialValue和CryptKey修改加密钥。
 *       public static string Encrypt(string originalText)
 *       public static string Decrypt(string cryptograph)
 * 
 * 作者：郝  伟
 * 
 * 时间：2011/06/02   初步建立这个类。
 *			  2011/06/17	加入判断，或加密或解密字符串为空，返回也为空。
 *			  2018/12/29   进行修改，删除了一些无用的内容。
 * 
 * 更新：
 * 
 * 
 */

using System;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Runtime.InteropServices;

namespace WindGoes6.Security
{
	/// <summary>
	/// 用于系统数据的加密或解密，提供了Encrypt和Decrypt两个函数用于加密和解密。
	/// 密码用到两个参数：InitialValue和CryptKey都可以进行修改。
	/// </summary>
	public class DESCrypto 
    {
		/// <summary>
		/// 加密计算的初始化向量。
		/// </summary>
		public static string InitialValue { get; set; } = "d02l19cx";

		/// <summary>
		/// 加密计算的密钥。
		/// </summary>
		public static string CryptKey { get; set; } = "y0M82Sq9";
       
        /// <summary>
        /// 对指定字符串进行加密，如果字符串为null或长度为0，返回null.
        /// </summary>
        /// <param name="inputString">输入字符串。</param>
        /// <returns></returns>
        public static string Encrypt(string inputString)
        {
			if(string.IsNullOrEmpty(inputString))
				return null;
			
            byte[] bytes = Encoding.UTF8.GetBytes(CryptKey);
            byte[] rgbIV = Encoding.UTF8.GetBytes(InitialValue);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, new DESCryptoServiceProvider().CreateEncryptor(bytes, rgbIV), CryptoStreamMode.Write);
            StreamWriter writer = new StreamWriter(cs);
            writer.Write(inputString);
            writer.Flush();
            cs.FlushFinalBlock();
            writer.Flush();
            return Convert.ToBase64String(ms.GetBuffer(), 0, (int)ms.Length);
        }

        /// <summary>
        /// 对指定字符串进行解密。
        /// </summary>
        /// <param name="cryptedString">加密字符串。</param>
        /// <returns></returns>
        public static string Decrypt(string cryptedString)
        {
			if (string.IsNullOrEmpty(cryptedString))
				return null;

			byte[] bytes = Encoding.UTF8.GetBytes(CryptKey);
            byte[] rgbIV = Encoding.UTF8.GetBytes(InitialValue);
            DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
            MemoryStream stream = new MemoryStream(Convert.FromBase64String(cryptedString));
            CryptoStream stream2 = new CryptoStream(stream, provider.CreateDecryptor(bytes, rgbIV), CryptoStreamMode.Read);
            StreamReader reader = new StreamReader(stream2);
            return reader.ReadToEnd();
        }
    }
}
