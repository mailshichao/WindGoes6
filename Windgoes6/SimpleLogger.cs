﻿/**
 * 
 * 2020/07/02 新建此类，提供了唯一的方法 void Log(string content, string path = "system.log")
 * 
 * */
using System;
using System.IO;
using System.Text;

namespace WindGoes6.Utils
{
	/// <summary>
	/// 简单的日志类，适合一般日志写入频率不高的应用场景，一般写入时间是秒级的。
	/// 核心方法：Log(content, path);
	/// </summary>
	public class SimpleLogger
	{
		/// <summary>
		/// 向指定的文件追加日志，格式为 2020/07/02 21:52:25.1392 这是测试日志的内容。
		/// 
		/// </summary>
		/// <param name="content"></param>
		/// <param name="path"></param>
		public static void Log(string content, string path = "system.log")
		{
			using (StreamWriter sw = new StreamWriter(path, true, Encoding.UTF8))
			{
				sw.WriteLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.ffff") + " " + content);
			}
		}
	}
}
