﻿using System.Collections.Generic;

namespace WindGoes6.Utils
{
	/// <summary>
	/// 用于表示调用DOS程序的执行结果。
	/// </summary>
	public class DosInvokerResult
	{
		/// <summary>
		/// 第一行输出数据。
		/// </summary>
		public List<string> Lines { get; internal set; } = new List<string>();

		/// <summary>
		/// 是否正常执行完成。
		/// </summary>
		public bool Successful { get; internal set; } = false;

		/// <summary>
		/// 退出码。-1表示异常，0表示正常。
		/// </summary>
		public int ExitCode { get; internal set; } = -1;

		/// <summary>
		/// 执行时长(秒)，默认值为-1。
		/// </summary>
		public double ExecutionTime { get; internal set; } = -1;
	}
}